<?php

namespace Renegade\MailChimpBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements  ConfigurationInterface
{
    /**
     * {@inheritDoc}
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('renegade_mailchimp');

        $rootNode->children()
            ->scalarNode('api_key')
                ->info('MailChimp API key')
                ->isRequired()->cannotBeEmpty()
            ->end()
            ->scalarNode('connection')
                ->info('Supported connections: http, https, stub')
                ->defaultValue('renegade.mailchimp.connection.https')
                ->beforeNormalization()
                    ->ifInArray(array('https', 'http', 'stub'))
                    ->then(function ($v) {
                        return sprintf('renegade.mailchimp.connection.%s', $v);
                    })
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}