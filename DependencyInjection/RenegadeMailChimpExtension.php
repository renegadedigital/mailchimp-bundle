<?php

namespace Renegade\MailChimpBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;


class RenegadeMailChimpExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $processor = new Processor();
        $config    = $processor->processConfiguration(new Configuration(), $configs);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('mail_chimp.xml');

        $container->setParameter('renegade.mailchimp.api_key', $config['api_key']);
        $container->setParameter('renegade.mailchimp.connection', $config['connection']);

        $container->setAlias('renegade.mailchimp.connection', $config['connection']);
    }

    /**
     * {@inheritdoc}
     */
    public function getAlias()
    {
        return 'renegade_mail_chimp';
    }

    /**
     * @codeCoverageIgnore
     */
    public function getXsdValidationBasePath()
    {
        return __DIR__ . '/../Resources/config/schema';
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespace()
    {
        return 'http://www.renegadedigital.com/schema/dic/mailchimp_bundle';
    }
}