<?php

/*
 * Renegade MailChimp bundle for Symfony2
 */
namespace Renegade\MailChimpBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RenegadeMailChimpBundle extends Bundle
{
}
