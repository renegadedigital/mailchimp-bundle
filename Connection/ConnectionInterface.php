<?php

namespace Renegade\MailChimpBundle\Connection;

use Renegade\MailChimpBundle\API\Request;
use Renegade\MailChimpBundle\API\Response;

/**
 * Interface that must be implemented by the connection classes
 */
interface ConnectionInterface
{
    /**
     * Executes the given request
     *
     * @param  Request $request
     *
     * @return Response
     */
    function execute(Request $request);
}